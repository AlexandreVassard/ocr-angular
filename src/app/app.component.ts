import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  appareilOne = 'Machine à laver';
  appareilTwo = 'Frigo';
  appareilThree = 'Ordinateur';
  title = 'Mon appli';
  isAuth = false;
  onAllumer() {
    alert('On allume tout !');
  }
  constructor() {
    setTimeout(() => {
      this.isAuth = true;
    }, 4000);
  }
}
